//soal 1
console.log("LOOPING PERTAMA");
var i = 2;
while(i <= 20){
    console.log(i+ " - I love coding");
    i+=2;
}

console.log("LOOPING KEDUA");
while(i >= 2){
    console.log(i+ " - I will become a frontend developer");
    i-=2;
}

//soal 2
console.log("\nNomor 2");
for(i = 1; i <=20; i++){
    if(i%2 == 1){
        if(i%3 == 0) {
            console.log(i + " - I Love Coding");
        }else{
         console.log(i+ " - Santai");
        }
    }else{
        console.log(i+ " - Berkualitas");
    }
}

//soal 3
console.log("\nNomor 3");
for(i = 0; i < 7; i++){
    var show = '';
    for (j = 0; j <= i; j++) {
        show += ' #';
    }
    console.log(show);
}

//soal 4
console.log("\nNomor 4");
var kalimat = "saya sangat senang belajar javascript";
var arr = kalimat.split(' ');
console.log(arr);

//soal 5
console.log("\nNomor 5");
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
daftarBuah.sort();
for (i = 0; i < daftarBuah.length; i++) {
    console.log(daftarBuah[i]);
}

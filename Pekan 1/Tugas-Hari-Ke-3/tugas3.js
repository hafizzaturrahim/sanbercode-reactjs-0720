//soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

//jawaban soal 1
console.log('1. ' +kataPertama+ " " +kataKedua.charAt(0).toUpperCase() + kataKedua.slice(1)+ " " +kataKetiga+ " "+kataKeempat.toUpperCase());

//soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

//jawaban soal 2
console.log('2. Jumlah : ' + (parseInt(kataPertama) + parseInt(kataKedua) + parseInt(kataKetiga) + parseInt(kataKeempat)));

//soal 3
var kalimat = 'wah javascript itu keren sekali'; 

//jawaban soal 3
var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); // do your own! 
var kataKetiga = kalimat.substring(15, 18); // do your own! 
var kataKeempat = kalimat.substring(19, 24); // do your own! 
var kataKelima = kalimat.substring(25, 31); // do your own! 

console.log('3. '); 
console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

//soal 4
var nilai = 69;
//jawaban soal 4
if (nilai >= 80) {
  	console.log('4. Indeks A'); 
}else if (nilai >= 70 && nilai < 80) {
  	console.log('4. Indeks B'); 
}else if (nilai >= 60 && nilai < 70) {
	console.log('4. Indeks C');
}else if (nilai >= 50 && nilai < 60) {
	console.log('4. Indeks D');
}else{
	console.log('4. Indeks E');
}

//soal 5
var tanggal = 24;
var bulan = 4;
var tahun = 1996;

//jawaban soal 5
switch(bulan){
	case 1 :  { console.log('5. ' + tanggal + ' Januari ' + tahun); break; }
	case 2 :  { console.log('5. ' + tanggal + ' Februari ' + tahun); break; }
	case 3 :  { console.log('5. ' + tanggal + ' Maret ' + tahun); break; }
	case 4 :  { console.log('5. ' + tanggal + ' April ' + tahun); break; }
	case 5 :  { console.log('5. ' + tanggal + ' Mei ' + tahun); break; }
	case 6 :  { console.log('5. ' + tanggal + ' Juni ' + tahun); break; }
	case 7 :  { console.log('5. ' + tanggal + ' Juli ' + tahun); break; }
	case 8 :  { console.log('5. ' + tanggal + ' Agustus ' + tahun); break; }
	case 9 :  { console.log('5. ' + tanggal + ' September ' + tahun); break; }
	case 10 :  { console.log('5. ' + tanggal + ' Oktober ' + tahun); break; }
	case 11 :  { console.log('5. ' + tanggal + ' November ' + tahun); break; }
	case 12 :  { console.log('5. ' + tanggal + ' Desember ' + tahun); break; }
	default :  { console.log('5. bulan tidak terdeteksi'); }
}
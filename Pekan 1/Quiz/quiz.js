function lingkaran(r){
    return 3.14 * r * r;
}

function segitiga(a,t){
    return 0.5 * a * t;
}

function persegi(s){
    return s * s;
}

console.log(segitiga(4,7));

var daftarAlatTulis = ["2. Pensil", "5. Penghapus", "3. Pulpen", "4. Penggaris", "1. Buku"];

daftarAlatTulis.sort();
var i = 0;
while(i < daftarAlatTulis.length){
    console.log(daftarAlatTulis[i]);
    i++;
}

function tampilkanBintang(jumlahBintang){
    for(i = jumlahBintang; i > 0; i--){
        var show = '';
        for (j = 0; j < i; j++) {
            show += ' *';
        }
        console.log(show);
    }
}

tampilkanBintang(7);

function panggilan(jenisKelamin, nama){
    if(jenisKelamin == 'L'){
        return "bapak " +nama;
    }else{
        return "ibu " +nama;
    }
}

console.log(panggilan("L","hafizh"));

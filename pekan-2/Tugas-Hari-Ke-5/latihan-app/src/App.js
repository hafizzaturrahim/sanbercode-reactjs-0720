import React from 'react';
import logo from './logo.svg';
import './App.css';
import './buah.css';

function App() {
  return (
    <div className="App">
	<section>
		<center>
			<h1>Form Pembelian Buah</h1>
		</center>

		<table>
			<tr>
				<td style={{width: '50%'}}><b>Nama Pelanggan</b></td>
				<td>
					<input type="text" name="fullname" />
				</td>
			</tr>
			<tr>
				<td style={{verticalAlign: "bottom",width:'30%'}}><b>Daftar Item</b></td>
				<td>
					<input type="checkbox" name="buah" value="Semangka"/>
					<label>Semangka</label><br/>
					<input type="checkbox" name="buah" value="Jeruk"/>
					<label>Jeruk</label><br/>
					<input type="checkbox" name="buah" value="Nanas"/>
					<label>Nanas</label><br/>
					<input type="checkbox" name="buah" value="Salak"/>
					<label>Salak</label><br/>
					<input type="checkbox" name="buah" value="Anggur"/>
					<label>Anggur</label><br/>
				</td>
			</tr>
			<tr>
				<td>
					<button className="custom" type="submit" name="">Kirim</button>
				</td>
			</tr>
		</table>

	</section>
    </div>
  );
}

export default App;

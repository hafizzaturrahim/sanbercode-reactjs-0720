//soal 1
console.log("soal 1");
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku", 1992];
var PesertaObj = {
  nama: arrayDaftarPeserta[0],
  jeniskelamin: arrayDaftarPeserta[1],
  hobi: arrayDaftarPeserta[2],
  tahunlahir: arrayDaftarPeserta[3],
};
console.log(PesertaObj);

//soal 2
console.log("\nsoal 2");
var buah = [
  {
    nama: "strawberry",
    warna: "merah",
    ada_bijinya: "tidak",
    harga: 9000,
  },
  {
    nama: "jeruk",
    warna: "oranye",
    ada_bijinya: "ada",
    harga: 8000,
  },
  {
    nama: "Semangka",
    warna: "Hijau & Merah",
    ada_bijinya: "ada",
    harga: 10000,
  },
  {
    nama: "Pisang",
    warna: "Kuning",
    ada_bijinya: "tidak",
    harga: 5000,
  },
];

console.log(buah[0]);

//soal 3
console.log("\nsoal 3");

var dataFilm = [];

function tambahFilm(namaFilm, durasiFilm, genreFilm, tahunFilm) {
  dataFilm.push({
    nama: namaFilm,
    durasi: durasiFilm,
    genre: genreFilm,
    tahun: tahunFilm,
  });
}
tambahFilm("Elf", "2 jam", "Horror", 2020);
tambahFilm("Bambang", "1 jam", "Action", 2017);
console.log(dataFilm);

//soal 4
console.log("\nsoal 4");
class Animal {
  property_band = 4;
  cold_blooded = false;
  legs = 4;
  constructor(name) {
    this.name = name;
  }
}

var sheep = new Animal("shaun");

console.log(sheep.name); // "shaun"
console.log(sheep.legs); // 4
console.log(sheep.cold_blooded); // false

class Frog extends Animal {
  jump() {
    console.log("hop hop");
  }
}

class Ape extends Animal {
  legs = 2;
  yell() {
    console.log("Auooo");
  }
}

var sungokong = new Ape("kera sakti");
sungokong.yell(); // "Auooo"

var kodok = new Frog("buduk");
kodok.jump(); // "hop hop"

//soal 5
console.log("\nsoal 5");

class Clock {
  constructor({ template }) {
    var timer;

    function render() {
      var date = new Date();

      var hours = date.getHours();
      if (hours < 10) hours = "0" + hours;

      var mins = date.getMinutes();
      if (mins < 10) mins = "0" + mins;

      var secs = date.getSeconds();
      if (secs < 10) secs = "0" + secs;

      var output = template
        .replace("h", hours)
        .replace("m", mins)
        .replace("s", secs);

      console.log(output);
    }

    this.stop = function () {
      clearInterval(timer);
    };

    this.start = function () {
      render();
      timer = setInterval(render, 1000);
    };
  }
}

var clock = new Clock({ template: "h:m:s" });
clock.start();

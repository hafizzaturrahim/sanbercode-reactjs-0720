var readBooksPromise = require("./promise.js");

var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]

// Lanjutkan code untuk menjalankan function readBooksPromise
function recursivePromiseRead(waktu, index) {
  readBooksPromise(waktu, books[index])
    .then(function (fulfilled) {
      if (index + 1 < books.length) { //cek agar array tidak out of bond
        console.log(fulfilled)
        recursivePromiseRead(fulfilled, index + 1);
      }
    })
    .catch(function (error) {
        //console.log(error);
    });
}

recursivePromiseRead(10000, 0);

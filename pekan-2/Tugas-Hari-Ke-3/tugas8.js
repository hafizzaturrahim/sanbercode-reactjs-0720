//soal 1
console.log('Nomor 1')
let luas = (r) => {
    const pi = 3.14
    return pi * r * r
}
let keliling = r => 2 * 3.14 * r

console.log(luas(7))
console.log(keliling(7))

//soal 2
console.log('\nNomor 2')

let kalimat = ""

function tambahKata(kalimat) {
    const kata = ['saya', 'adalah', 'seorang', 'frontend', 'developer']
    kalimat += `${kata[0]} ${kata[1]} ${kata[2]} ${kata[3]} ${kata[4]}`
    return kalimat
}

console.log(tambahKata(kalimat))

//soal 3
console.log('\nNomor 3')
class Book {
    constructor(name, totalPage, price) {
        this.name = name
        this.totalPage = totalPage
        this.price = price
    }
}

class Komik extends Book{
    constructor(name, totalPage, price, isColorful ){
        super(name, totalPage, price)
        this.isColorful = isColorful
    }
}

let manga = new Komik('Overflow',8,30000,true)
console.log(manga)